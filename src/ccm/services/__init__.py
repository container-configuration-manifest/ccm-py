

class DockerCompose(object):

    services = []
    """ A list of services this docker-compose definition contains. """

    volumes = []

    _docker_compose_file = None
    """ The docker-compose file this service was defined in. Used to prevent meaningful error messages."""

    def __init__(self, raw_content_dict, docker_compose_file=None):

        # Save the docker-compose file this data originates from for better error messages.
        self._docker_compose_file = docker_compose_file

        # Loop over the data of each service defined in the docker-compose file
        for s in raw_content_dict['services']:

            for e in s['environment']:
                pass

            # Instantiate a Service() object for every service defined in the docker-compose file
            # and add it to the list of services.
            self.services.append(Service(s['image'], s, ))


class Service(object):
    """
        This class represents a service loaded from a docker-compose.yml.

        Documentation on how services are structured can be found here: https://docs.docker.com/get-started/part3/.
    """

    name = None
    """ The name of the service defined in it's docker-compose.yml file. """

    image = None
    """ Reference to the docker image used by this service. """

    environment_variables = []
    """ A list of environment variables defined for the service. """

    volumes = {}
    """ A dict containing volumes and the path they are mapped to. """

    def __init__(
            self,
            image,
            name=None,
            environment_variables=None,
            volumes=None
    ):
        self.name = name
        self.image = image
        self.environment_variables = environment_variables
        self.volumes = volumes


