from ccm.utils import FileLoader, URLLoader, YMLParser


class Manifest(object):
    """
        This class represents a manifest.
    """

    def __init__(self, file=None, url=None):
        if (not file and not url) or (file and url):
            raise ValueError("Either 'file' or 'value' argument is required")

        if file:
            self._raw = FileLoader(file).read()
        elif url:
            self._raw = URLLoader(url).read()

        if self._raw:
            self.raw_content = YMLParser.parse(self._raw)

    def _parse(self):
        pass
