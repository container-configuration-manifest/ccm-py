class ServiceValidationError(Exception):
    pass


class ManifestValidationError(Exception):
    pass


class TemplateValidationError(Exception):
    pass
