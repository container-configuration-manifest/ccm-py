
def validate(sources):
    """
    This function loads the content from the passed files and urls and serializes the content.
    Any manifests or templates will be validated after serializing.
    Any services found in the provided docker-compose files will be validated against any compatible manifest.

    This allows you to parse any number of docker-compose files, manifest-files & -urls.

    :param sources: A list of yml file sources (urls or paths).
    :raises ValidationError: if validation failed.
    :raises ManifestValidationError: if one of the provided manifests can't be validated.
    :raises ValueError: if one of the parameters not lists of strings
    :raises FileNotFoundError: if any of the passed in files can't be located.
    :return: Whether the validation was successful
    :rtype: bool
    """

    print(sources)

    raise NotImplementedError()


