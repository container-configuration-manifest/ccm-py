

class YMLParser(object):
    """
        Parsing yml formatted text.
    """

    raw_content = None

    def __init__(self, raw_content):
        pass

    def parse(self):
        """
        Converts the instances raw_content to a dict.

        :return: The converted raw_content.
        """
        pass


class FileLoader(object):
    """
        This utility class is responsible for opening **files** and reading there content.
    """

    file = None

    def __init__(self, file):
        pass

    def read(self):
        """
        Reads the content of the instances file.

        :return: The files content as text.
        """
        pass


class URLLoader(object):
    """
        This utility class is responsible for opening **urls** and reading there content.
    """

    url = None

    def __init__(self, url):
        pass

    def read(self):
        """
        Reads the content of the instances url.

        :return: The content loaded from the url as text.
        """
        pass
