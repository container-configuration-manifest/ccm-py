from setuptools import setup
import os

setup(
      name='ccm-py',
      version=os.getenv('VERSION_TAG', ''),
      description='Container Configuration Manifest Validation',
      url='https://gitlab.com/container-configuration-manifest/ccm-py',
      author='Sven Fritsch',
      author_email='sven@fritsch.io',
      license='MIT',
      packages=['ccm'],
      zip_safe=False
)
