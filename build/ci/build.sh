#!/bin/bash

set -o errexit
set -o pipefail

docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
BUILD_IMAGE_BASE="$CI_REGISTRY/$CI_PROJECT_PATH"


# Build Image
docker build --pull -t "$BUILD_IMAGE_BASE:latest" "src"
docker push "$BUILD_IMAGE_BASE:latest"
