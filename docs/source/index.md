.. Container Configuration Manifest documentation master file, created by
   sphinx-quickstart on Tue Mar 26 22:14:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ccm-py's documentation!
=====================================================================

**Container Configuration Manifest**

This package provides a way to validate your containers configuration against one or multiple manifests.

.. toctree::
   :maxdepth: 5
   
   concept
   installation
   usage
