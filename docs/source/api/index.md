API
****

Contents
========

.. toctree::
   :maxdepth: 3
   
   ccm
   manifests
   services
   exceptions
   utils
   
Usage
=====

Manifest Structure
------------------


Examples
--------

Validation
___________

The way ccm's validation works is pretty generic.
You pass in one or many .yml files from which the ccm extracts, validates and serializes all manifests and services it can find.

After that it loops over all services that were extracted and validates them against every manifest that matches it's image.

First we will create a manifest definition in a YAML file:

.. code:: yaml

    # my_manifest.ccm.yml
    
    manifests:
        
        app:
            image: my_username/my_image
            environment:
                DEBUG:
                    default: NO
                    type: bool
                MODE:
                    default: testing
                    options:
                        - testing
                        - staging
                        - production
                CACHING:
                    HOST:
                        required: YES
                        type: str
                        options: any

After that we create a YAML file containing a service (usually a docker-compose.yml)

**Note:** The services :code:`image` property matches the one of our manifest.

.. code:: yaml

    # staging/docker-compose.yml
    
    version: '3'
    
    services:
        
        app:
            image: my_username/my_image
            environment:
                DEBUG: YES
                CACHING_HOST: 'cache@my-host.com:3456'
    

The last step is to create a python file that calls :code:`ccm.validate()` and passes our two YAML files as parameters.

.. code:: python
    
    # my_python_script.py
    
    import os
    import ccm
    
    try:
        services, manifests = ccm.parse([
            os.path.join(base_dir, 'staging/docker-compose.yml'), 
            os.path.join(base_dir, 'my_manifest.ccm.yml')
        ])
        is_valid = ccm.validate(services, manifests)
        
    except ValueError, ServiceValidationError, ManifestValidationError as e:
        print(e)
     

Generating Documentation
________________________

Besides validating your manifests, the ccm can also generate human readable documentation for you.
Just parse in a manifest and a output folder path into :code:`ccm.generate_docs()` 
and it will generate a file called :code:`CONTAINER_CONF.md` for you.

.. code:: python
    
    # my_python_script.py
    
    import os
    import ccm
    
    base_dir = os.path.dirname(os.path.abspath(__file__))
    
    try:
        services, manifests = ccm.parse([os.path.join(base_dir, 'my_manifest.ccm.yml')])
        ccm.generate_docs(
            manifests[0],
            output_dir=os.path.join(base_dir, 'docs')
        )
    except FileNotFoundError, ServiceValidationError, ManifestValidationError as e:
        print(e)


Generating a valid Service
___________________

To generate a service from a manifest just parse the manifest to :code:`ccm.generate_service()` to get a minimal, valid
service configuration from the manifest. If you want a more advanced configuration just pass :code:`full=True`.

.. code:: python
    
    # my_python_script.py
    
    import os
    import ccm
    
    base_dir = os.path.dirname(os.path.abspath(__file__))
    
    try:
        services, manifests     = ccm.parse([os.path.join(base_dir, 'my_manifest.ccm.yml')])
        new_service             = ccm.generate_service(manifests[0], full=True)
        
        print(new_service)
        
    except FileNotFoundError, ServiceValidationError, ManifestValidationError as e:
        print(e)